

// https://ethereum.stackexchange.com/questions/3542/how-are-ethereum-addresses-generated/3619#3619


import Foundation
import CSecp256k1
import keccak256
import HexString

public enum EthereumKeyError : Error {
    case WrongSeedSize
}

extension EthereumKeyError: CustomStringConvertible {
    public var description: String {
        switch self {
        case .WrongSeedSize:
            return "WrongSeedSize"
        }
    }
}


public class EthereumKey {
    
    fileprivate let priv: [UInt8] // 32 bytes
    fileprivate let pub: [UInt8] // 64 bytes
    fileprivate let acct: String // 20 bytes (represented as a 0x prefixed hex string)
    
    fileprivate var ctx: OpaquePointer!
    
    public init(_ seed: [UInt8]) throws
    {
        if (seed.count != 32) { throw EthereumKeyError.WrongSeedSize }
        self.ctx = secp256k1_context_create(UInt32(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY))
        priv = seed
        let rawPub = [UInt8](repeating: 0, count: 64)
        let pubKey :UnsafeMutablePointer<secp256k1_pubkey> = UnsafeMutableRawPointer(mutating: rawPub).bindMemory(to: secp256k1_pubkey.self, capacity: 1)
        _ = secp256k1_ec_pubkey_create(ctx!, pubKey, seed)
        var pk = [UInt8](repeating: 0, count: 65)
        var count: Int = 65
        _ = secp256k1_ec_pubkey_serialize(ctx!, &pk, &count, pubKey, UInt32(SECP256K1_EC_UNCOMPRESSED))
        self.pub = [UInt8](pk[1..<65])
        acct = EthereumKey.account(self.pub)
    }
    
    public convenience init?(seed: [UInt8]) {
        do {
            try self.init(seed)
        } catch {
            return nil
        }
    }
    
    public var pubKey: [UInt8] {
        get {
            return pub
        }
    }
    
    public var account: String {
        get {
            return acct
        }
    }
    
    
    public func sign(_ message: [UInt8]) -> [UInt8] {// the recoverable sig is 64 bytes for the sig followed by 1 recovery byte
        let rawSig = [UInt8](repeating: 0, count: 65)
        
        let signature :UnsafeMutablePointer<secp256k1_ecdsa_recoverable_signature> = UnsafeMutableRawPointer(mutating: rawSig).bindMemory(to: secp256k1_ecdsa_recoverable_signature.self, capacity: 1)

        _ = secp256k1_ecdsa_sign_recoverable(ctx!, signature, message, self.priv, nil, nil) 
        var recid: Int32 = 0
        var sig = [UInt8](repeating: 0, count: 64)
        
        _ = secp256k1_ecdsa_recoverable_signature_serialize_compact(ctx!, &sig, &recid, signature)
        
        sig.append(UInt8(recid))
        return sig
    }
    
    public static func verify(_ publicKey: [UInt8], _ message: [UInt8], _ sig: [UInt8]) -> Bool {
        let ctx = secp256k1_context_create(UInt32(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY))
        let rawSig = [UInt8](repeating: 0, count: 65)
        let signature :UnsafeMutablePointer<secp256k1_ecdsa_recoverable_signature> = UnsafeMutableRawPointer(mutating: rawSig).bindMemory(to: secp256k1_ecdsa_recoverable_signature.self, capacity: 1)
        
        _ = secp256k1_ecdsa_recoverable_signature_parse_compact(ctx!, signature, [UInt8](sig[0..<64]), Int32(sig[64]))
        
        let rawPk = [UInt8](repeating: 0, count: 64)
        let pubKey :UnsafeMutablePointer<secp256k1_pubkey> = UnsafeMutableRawPointer(mutating: rawPk).bindMemory(to: secp256k1_pubkey.self, capacity: 1)
        
        _ = secp256k1_ecdsa_recover(ctx!, pubKey, signature, message)
        
        var pk = [UInt8](repeating: 0, count: 65)
        var count: Int = 65
        _ = secp256k1_ec_pubkey_serialize(ctx!, &pk, &count, pubKey, UInt32(SECP256K1_EC_UNCOMPRESSED))

        return [UInt8](pk[1..<65]) == publicKey
    }
    
    public static func account( _ publicKey: [UInt8]) -> String {
        let r: [UInt8] = keccak256(publicKey)
        let slice = r.index(after: 11)..<r.endIndex
        return r[slice].reduce("", { (r, i) -> String in
            return r + String(format: "%02X", i)
        })
    }
    
    public static func account( _ publicKey: String) -> String {
        if let pub = publicKey.toByteArray() {
            return EthereumKey.account(pub)
        }
        return "0000000000000000000000000000000000000000"
    }
}


extension EthereumKey {
    public var pubKeyStr: String {
        get {
            return self.pub.reduce("", { (r, i) -> String in
                return r + String(format: "%02X", i)
            })
        }
    }
    
    public func signAsStr(_ message: [UInt8]) -> String {
        return self.sign(message).reduce("", { (r, i) -> String in
            return r + String(format: "%02X", i)
        })
    }
    
    public var accountHexStr: String {
        get {
            return "0X" + self.account
        }
    }
}
