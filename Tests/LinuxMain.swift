import XCTest
@testable import EthereumKeysTests

XCTMain([
    testCase(EthereumKeysTests.allTests),
])
