// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "EthereumKeys",
    products: [
      .library(name: "EthereumKeys", targets: ["EthereumKeys"])
    ],
    dependencies: [
        .package(url: "https://gitlab.com/katalysis/open-source/CSecp256k1.git", from: "0.2.2"),
        .package(url: "https://gitlab.com/katalysis/open-source/HexString.git", from: "0.2.0"),
        .package(url: "https://gitlab.com/katalysis/open-source/keccak256.git", from: "1.1.3"),
        
   ],
   targets: [
     .target(name: "EthereumKeys", dependencies: ["HexString", "keccak256"], path: ".", sources: ["Sources"]),
     .testTarget(name: "EthereumKeysTests", dependencies: ["EthereumKeys"])
   ]
)
